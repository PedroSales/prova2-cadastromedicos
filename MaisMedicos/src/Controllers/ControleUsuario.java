package Controllers;

import java.util.ArrayList;

import Models.Medico;
import Models.Paciente;

public class ControleUsuario {

ArrayList <Paciente> listaPaciente;
ArrayList <Medico> listaMedico;
	
	
	public ControleUsuario(){
		listaPaciente = new ArrayList<Paciente>();
		listaMedico = new ArrayList<Medico>();
	}
	
	public void adicionarPaciente (Paciente umPaciente){
		listaPaciente.add(umPaciente);
	}
	
	public void removerPaciente (Paciente umPaciente){
		listaPaciente.remove(umPaciente);
	}
	
	public Paciente buscarPaciente (String umNome){
		for(Paciente umPaciente : listaPaciente){
			if(umPaciente.getNome().equalsIgnoreCase(umNome));
			return umPaciente;
		}
		
		return null;
	}
	
	
	public void adicionarMedico (Medico umMedico){
		listaMedico.add(umMedico);
	}
	
	public void removerMedico (Medico umMedico){
		listaMedico.remove(umMedico);
	}
	
	public Medico buscarMedico (String umNome){
		for(Medico umMedico : listaMedico){
			if(umMedico.getNome().equalsIgnoreCase(umNome));
			return umMedico;
		}
		
		return null;
	}
}

