package Controllers;

import java.util.ArrayList;

import Models.Consulta;
import Models.Medico;
import Models.Paciente;

public class ControleConsulta {
	
		ArrayList<Paciente> listaConsulta;
		
		public ControleConsulta(){
			listaConsulta = new ArrayList<Paciente>();
		}

		public void MarcaConsulta(String Nome, String Data, String Hora, Medico Medico){
			Paciente umPaciente = new Paciente(Nome);
			Consulta umaConsulta = new Consulta(Data);
			umaConsulta.setHora(Hora);
			umaConsulta.setMedico(Medico);
			
			umPaciente.setConsulta(umaConsulta);
			
			listaConsulta.add(umPaciente);
		}
		
		public void CancelaConsulta(String Nome){
			Paciente umPaciente = BuscaPaciente(Nome);
			
			if(umPaciente != null){
			listaConsulta.remove(umPaciente);
			}
			
		}
		
		public Paciente BuscaPaciente(String Nome){
			for(Paciente umPaciente: listaConsulta){
				if(umPaciente.getNome().equalsIgnoreCase(Nome)){
					return umPaciente;
				}
			}
			return null;
		}
		
		public void PrescreveMedicamento(String Medicamento, String Nome){
			Paciente umPaciente = BuscaPaciente(Nome);
			if(umPaciente!= null){
			umPaciente.setMedicamento(Medicamento);
			}
		}
		
		
		
}
