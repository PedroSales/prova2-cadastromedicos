package Models;

public class Consulta {
	
	private String Data;
	private String Hora;
	private Medico Medico;
	
	public Consulta (String Data){
		this.Data = Data;
	}
	
	
	public String getData() {
		return Data;
	}
	public void setData(String data) {
		Data = data;
	}
	public String getHora() {
		return Hora;
	}
	public void setHora(String hora) {
		Hora = hora;
	}
	public Medico getMedico() {
		return Medico;
	}
	public void setMedico(Medico medico) {
		Medico = medico;
	}
	
	

}
