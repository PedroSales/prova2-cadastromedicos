package Models;

public class Medico extends Usuario{
	
	private String CRM;
	private String cadastro;
	private String DataEntrada;
	private String Especialidade;
	
	public Medico (String Nome, String CRM){
		super(Nome);
		this.CRM = CRM;
	}
	
	
	public String getCRM() {
		return CRM;
	}
	public void setCRM(String cRM) {
		CRM = cRM;
	}
	public String getCadastro() {
		return cadastro;
	}
	public void setCadastro(String cadastro) {
		this.cadastro = cadastro;
	}
	public String getDataEntrada() {
		return DataEntrada;
	}
	public void setDataEntrada(String dataEntrada) {
		DataEntrada = dataEntrada;
	}
	public String getEspecialidade() {
		return Especialidade;
	}
	public void setEspecialidade(String especialidade) {
		Especialidade = especialidade;
	}
	
	

}
